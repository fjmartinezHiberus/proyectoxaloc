<?php
//clase controlador
abstract class Controlador
{
    // Cargar modelo
    public function modelo($modelo) {
        //carga
        require_once '../app/modelos/' . $modelo . '.php';
        // intanciar el modelo
        return new $modelo();
    }

    // Cargar Vista
    public function vista($vista, $datos = []) {
        //checkear si el arcivo vista existe
        if (file_exists('../app/vistas/' . $vista . '.php')) {
            require_once '../app/vistas/' . $vista . '.php';
            // intanciar el modelo
            return new $vista();
        }
        else {
            //error
            die('La vista no existe');
        }


    }
}
