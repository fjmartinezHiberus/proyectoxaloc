<?php
/* Mapear la url ingresada en el navegador,
   1-controlador
   2-método
   3-parametro

    Ejemplo: /articulos/actualizar/4
*/

class Core
{
    protected $controladorActual = 'Vehicles';
    protected $metodoActual = 'index';
    protected $parametros = [];

    public function __construct()
    {
        $url = $this->getUrl();
        if (file_exists('../controladores'). ucwords($url[0]. '.php')) {
            # code ..
            $this->controladorActual = ucwords($url[0]);
            unset($url[0]);
        }
        // requerir el controlador
        require_once '../app/controladores/' . $this->controladorActual. '.php';
        $this->controladorActual = new $this->controladorActual;

        // verificar la segunda parte de la url que seria el metodo
        if (isset($url[1])) {
            if (method_exists($this->controladorActual, $url[1])) {
                // checkeamos el metodo
                $this->metodoActual = $url[1];
                unset($url[1]);            }
        }
        // obtener el parametros numerico

        $this->parametros = $url ? array_values($url) : [];
        // function call back

       call_user_func_array([$this->controladorActual, $this->metodoActual], $this->parametros);
    }

    public function getUrl()
    {
        // si esta seteada la url
        if (isset($_GET['url']))
        {
            $url = rtrim($_GET['url'], '/');
            $url = filter_var($url, FILTER_SANITIZE_URL);
            $url = explode('/', $url);
            return $url;
        }
    }
}
