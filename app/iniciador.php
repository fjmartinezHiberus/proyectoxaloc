<?php
// Cargar librerias
require_once 'config/configurar.php';

// AutoLoad para hacer la carga no manualmente
spl_autoload_register(function($nombreClase){
    require_once 'librerias/' . $nombreClase. '.php' ;
});
