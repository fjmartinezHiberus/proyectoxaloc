<?php

class Vehicles extends Controlador
{
    public function __construct()
    {
        $this->VehiclesModelo = $this->modelo('Vehicle');
    }

    public function index()
    {
        $vehicles = $this->VehiclesModelo->getVehicles();
        $datos = [
            'title' => 'List Vehicles',
            'model' => 'vehicles',
            'vehicles' => $vehicles
        ];
        $this->vista('vehicles/inicio', $datos);
    }

    public function delete($params) {
        $vehicles = $this->VehiclesModelo->deleteVehicles($params);
        exit(200);
        $this->index();
    }

    public function create($params) {
        if (isset($params) && $params>0) {
            $vehicle = $this->VehiclesModelo->getVehiclesId($params);
            $datos = [
                'title' => 'Modify Vehicle',
                'model' => 'vehicles',
                'vehicle' => json_decode(json_encode($vehicle), true)
            ];
            $this->vista('vehicles/modify', $datos);
        }
        else {
            $datos = [
                'title' => 'New Vehicle',
                'model' => 'vehicles'
            ];
            $this->vista('vehicles/create', $datos);
        }

    }

    public function createBdd($params) {
        $params = [];
        $params['brand'] = $_REQUEST['Brand'];
        $params['model'] = $_REQUEST['Model'];
        $params['plate'] = $_REQUEST['Plate'];
        $params['LicenseRequired'] = $_REQUEST['LicenseRequired'];
        $crear = $this->VehiclesModelo->createVehicle($params);
        exit(200);
    }

    public function modify($params) {
        $vehicle = $this->VehiclesModelo->getVehiclesId($params);
        print_R($vehicle->Id);die;
        exit(200);
    }


    public function modifyBdd($params) {
        $params = [];
        $params['Id'] = $_REQUEST['Id'];
        $params['brand'] = $_REQUEST['Brand'];
        $params['model'] = $_REQUEST['Model'];
        $params['plate'] = $_REQUEST['Plate'];
        $params['LicenseRequired'] = $_REQUEST['LicenseRequired'];
        $crear = $this->VehiclesModelo->modifyVehicle($params);
        exit(200);
    }

}

