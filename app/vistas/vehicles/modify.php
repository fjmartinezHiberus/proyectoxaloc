<?php require RUTA_APP . '/vistas/inc/header.php'; ?>
<h2><?php echo $datos['title'];?></h2>
<form id="modifyVehicle"  name="modifyVehicle" method="post">
<input type="hidden" id="id" name="id" value="<?php echo $datos['vehicle']["Id"]?>" />
    <ul>
        <li>
            <label for="Brand">Brand:</label>
             <input type="text" id="brand" name="brand" required value="<?php echo $datos['vehicle']["Brand"]?>"/> *
        </li>
        <li>
            <label for="mail">Model:</label>
            <input type="text" id="model" name="model" value="<?php echo $datos['vehicle']["Model"]?>" required /> *
        </li>
        <li>
            <label for="msg">Plate:</label>
            <input type="text" id="plate" name="plate"  value="<?php echo $datos['vehicle']["Plate"]?>" required /> *
        </li>
        <li>
            <label for="msg">License Required:</label>
            <input type="text" id="licenserequired" name="licenserequired"  value="<?php echo $datos['vehicle']["Licenserequired"]?>" maxlength="1" required /> *
        </li>

    </ul>
    <button class="buttonCreateVehicle" type="submit"><?php echo $datos['title'] ?></button>
</form>
<?php require RUTA_APP . '/vistas/inc/footer.php';?>