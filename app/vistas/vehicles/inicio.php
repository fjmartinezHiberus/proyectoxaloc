<?php require RUTA_APP . '/vistas/inc/header.php'; ?>
<h2><?php echo $datos['title'];?></h2>

<a href="<?php echo RUTA_URL . '/vehicles/create/0'?>" target="_blank">New Vehicle</a>
<ul>
    <?php foreach($datos['vehicles'] as $vehicle) : ?>
        <li><?php echo $vehicle->Brand?>
            <button class="buttonModify" data-id="<?php echo $vehicle->Id?>" data-operation="modify" data-model="<?php echo $datos['model']?>" onClick="operacion(this)">Modificar</button>
            <button class="buttonIndex" name="buttonIndex" data-id="<?php echo $vehicle->Id?>" data-operation="delete" data-model="<?php echo $datos['model']?>" onClick="operacion(this)">Eliminar</button>
        </li>
    <?php endforeach; ?>
</ul>
<?php require RUTA_APP . '/vistas/inc/footer.php';?>