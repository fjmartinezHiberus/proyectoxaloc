<?php require RUTA_APP . '/vistas/inc/header.php'; ?>
<h2><?php echo $datos['title'];?></h2>
<form id="createVehicle"  name="createVehicle" method="post">
    <ul>
        <li>
            <label for="Brand">Brand:</label>
             <input type="text" id="brand" name="brand" required "/> *
        </li>
        <li>
            <label for="mail">Model:</label>
            <input type="text" id="model" name="model" required /> *
        </li>
        <li>
            <label for="msg">Plate:</label>
            <input type="text" id="plate" name="plate" required /> *
        </li>
        <li>
            <label for="msg">License Required:</label>
            <input type="text" id="licenserequired" name="licenserequired"  maxlength="1" required /> *
        </li>

    </ul>
    <button class="buttonCreateVehicle" type="submit"><?php echo $datos['title'] ?></button>
</form>
<?php require RUTA_APP . '/vistas/inc/footer.php';?>