<?php

class Vehicle {
    private $db;

    public function __construct()
    {
        $this->db = new Base;
    }

    public function getVehicles() {
        $this->db->query("SELECT * FROM VEHICLES");
        return $this->db->registros();
    }

    public function getVehiclesId($id) {
        $this->db->query("SELECT * FROM VEHICLES WHERE Id=:Id");
        $this->db->bind(':Id', $id);
        $resultado = $this->db->execute();
        if($resultado === TRUE)
        {
            return $this->db->registro();
        }
    }

    public function deleteVehicles($id) {
        $this->db->query("DELETE FROM VEHICLES WHERE Id=:Id");
        $this->db->bind(':Id', $id);
        $resultado = $this->db->execute();
        if($resultado === TRUE)
        {
            return $this->getVehicles();
        }
    }

    public function createVehicle($params) {

        $this->db->query("INSERT INTO VEHICLES  (`Brand`, `Model`, `Plate`, `LicenseRequired`) VALUES (:Brand, :Model, :Plate, :LicenseRequired)");
        $this->db->bind(':Brand', $params['brand']);
        $this->db->bind(':Model', $params['model']);
        $this->db->bind(':Plate', $params['plate']);
        $this->db->bind(':LicenseRequired',  $params['LicenseRequired']);
        $resultado = $this->db->execute();
        if($resultado === TRUE)
        {
            return $this->getVehicles();
        }
    }

    public function modifyVehicle($params) {
        $this->db->query("UPDATE VEHICLES  SET Id=:Id, Brand=:Brand, Model=:Model, Plate=:Plate, LicenseRequired=:LicenseRequired  WHERE Id=:Id");
        $this->db->bind(':Id', $params['Id']);
        $this->db->bind(':Brand', $params['brand']);
        $this->db->bind(':Model', $params['model']);
        $this->db->bind(':Plate', $params['plate']);
        $this->db->bind(':LicenseRequired',  $params['LicenseRequired']);
        $resultado = $this->db->execute();
        if($resultado === TRUE)
        {
            return $this->getVehicles();
        }
    }

}
