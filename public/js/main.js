
$(document).ready(function() {
    $(".buttonIndex").click(function(e) {
        e.preventDefault();
        // TODO, confirmar operacion
        $.ajax({
            type: "POST",
            url: "/xaloc/public/" + $(this).attr('data-model')  + "/" + $(this).attr('data-operation') + "/" + $(this).attr('data-id'),
            data: {
                id: $(this).attr('data-id'), // < note use of 'this' here
            },
            success: function(result) {
                window.location.reload();
            },
            error: function(result) {
                alert('error');
            }
        });
    });

    $(".buttonModify").click(function(e) {

        e.preventDefault();
        $.ajax({
            type: "POST",
            url: "/xaloc/public/" + $(this).attr('data-model')  + "/" + $(this).attr('data-operation') + "/" + $(this).attr('data-id'),
            data: {
                id: $(this).attr('data-id'), // < note use of 'this' here
            },
            success: function(result) {
                location.href = "http://localhost/xaloc/public/vehicles/create/" + result;
            },
            error: function(result) {
                alert('error');
            }
        });
    });

    $("#createVehicle").submit(function (event) {
        event.preventDefault();
        var formData= {
            Brand: $("#brand").val(),
            Model: $("#model").val(),
            Plate: $("#plate").val(),
            LicenseRequired: $("#licenserequired").val(),
        };
        $.ajax({
            type: "POST",
            url: "/xaloc/public/vehicles/createBdd/0",
            data: formData,
            success: function(result) {
                //window.location.reload();
                window.location.href = "/xaloc/public/vehicles/";
            },
            error: function(result) {
                alert('error');
            }
        });
    });

    $("#modifyVehicle").submit(function (event) {
        event.preventDefault();
        var formData= {
            Id:  $('#id').val(),
            Brand: $("#brand").val(),
            Model: $("#model").val(),
            Plate: $("#plate").val(),
            LicenseRequired: $("#licenserequired").val(),
        };
        $.ajax({
            type: "POST",
            url: "/xaloc/public/vehicles/modifyBdd/"+ $('#id').val(),
            data: formData,
            success: function(result) {
                //window.location.reload();
                window.location.href = "/xaloc/public/vehicles/";
            },
            error: function(result) {
                alert('error');
            }
        });
    });
});


