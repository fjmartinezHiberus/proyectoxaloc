SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";

create database xalox;
use xalox;


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `xalox`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `DRIVERS`
--

CREATE TABLE `DRIVERS` (
  `Id` bigint(20) NOT NULL,
  `Name` varchar(255) NOT NULL,
  `Surname` varchar(255) NOT NULL,
  `License` varchar(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `TRIP`
--

CREATE TABLE `TRIP` (
  `Vehicle` bigint(20) NOT NULL,
  `Driver` bigint(20) NOT NULL,
  `Date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `VEHICLES`
--

CREATE TABLE `VEHICLES` (
  `Id` bigint(20) NOT NULL,
  `Brand` varchar(255) NOT NULL,
  `Model` varchar(255) NOT NULL,
  `Plate` varchar(255) NOT NULL,
  `LicenseRequired` varchar(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `DRIVERS`
--
ALTER TABLE `DRIVERS`
  ADD PRIMARY KEY (`Id`);

--
-- Indices de la tabla `TRIP`
--
ALTER TABLE `TRIP`
  ADD KEY `Vehicle_idx` (`Vehicle`),
  ADD KEY `Driver_Idx` (`Driver`);

--
-- Indices de la tabla `VEHICLES`
--
ALTER TABLE `VEHICLES`
  ADD PRIMARY KEY (`Id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `DRIVERS`
--
ALTER TABLE `DRIVERS`
  MODIFY `Id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `VEHICLES`
--
ALTER TABLE `VEHICLES`
  MODIFY `Id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `TRIP`
--
ALTER TABLE `TRIP`
  ADD CONSTRAINT `TRIP_ibfk_1` FOREIGN KEY (`Vehicle`) REFERENCES `VEHICLES` (`Id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `TRIP_ibfk_2` FOREIGN KEY (`Driver`) REFERENCES `DRIVERS` (`Id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
